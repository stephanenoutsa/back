<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_requests', function (Blueprint $table) {
            $table->id();
            $table->string('sender_name');
            $table->string('sender_email');
            $table->string('recipient_name');
            $table->string('recipient_phone');
            $table->string('payment_proof');
            $table->double('original_amount');
            $table->enum('original_currency', ['USD', 'ZAR', 'AED', 'XAF']);
            $table->double('converted_amount');
            $table->double('converted_amount_with_commission');
            $table->enum('status', ['pending', 'rejected', 'verified', 'processed']);
            $table->string('verification_proof')->nullable();
            $table->string('process_proof')->nullable();
            $table->string('reject_reason')->nullable();
            $table->timestamp('request_verified_at')->nullable();
            $table->timestamp('request_processed_at')->nullable();
            $table->timestamp('request_rejected_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_requests');
    }
}
