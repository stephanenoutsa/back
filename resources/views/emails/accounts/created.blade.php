@component('mail::message')
# Welcome {{ $name }}

An Account has been created for you on the {{ config('app.name') }} as a <b>{{ $role }}</b>.

You may login to the app by clicking the button below:

@component('mail::button', ['url' => config('app.admin')])
Login
@endcomponent

Your password is <b><u>{{ $password }}</u></b>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
