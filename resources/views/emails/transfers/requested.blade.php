@component('mail::message')
# Hi {{ $name }}

The user {{ $request->sender_name }} has requested a transfer of the amount of
{{ $request->original_amount }} {{ $request->original_currency}} ({{ $request->converted_amount }} {{ $request->target_currency }}).

You may check the request by clicking the button below:

@component('mail::button', ['url' => config('app.admin') . '/transfer-requests/' . $request->id])
View Request
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
