@component('mail::message')
# Hi {{ $name }}

The transfer request with ID {{ $id }} has been verified.

You may process the request by clicking the button below:

@component('mail::button', ['url' => config('app.admin') . '/transfer-requests/' . $id])
Process Request
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
