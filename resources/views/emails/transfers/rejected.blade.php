@component('mail::message')
# Hi {{ $name }}

The transfer request with ID {{ $request->id }} of the amount of {{ $request->original_amount }} {{ $request->original_currency }}
({{ $request->converted_amount }} {{ $request->target_currency }}) has been rejected.

This acknowledges that the transaction has been cancelled.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
