@component('mail::message')
# Hi {{ $request->sender_name }}

You have requested a transfer of the amount of
{{ $request->original_amount }} {{ $request->original_currency}} ({{ $request->converted_amount }} {{ $request->target_currency }}).

This amount will be sent to {{ $request->recipient_name }} through their number {{ $request->recipient_phone }}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
