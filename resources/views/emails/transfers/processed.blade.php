@component('mail::message')
# Hi {{ $name }}

The transfer request with ID {{ $request->id }} of the amount of {{ $request->original_amount }} {{ $request->original_currency }}
({{ $request->converted_amount }} {{ $request->target_currency }}) has been processed. See proof of payment below:

{{ $request->process_details }}

This acknowledges that the recipient has been sent the funds they were due.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
