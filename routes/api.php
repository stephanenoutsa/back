<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\TransferRequestController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login'])
    ->name('login');

Route::post('/logout', [AuthController::class, 'logout'])
    ->middleware('auth:sanctum');

Route::apiResource('/users', UserController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/transfer-requests', TransferRequestController::class);
Route::post('/transfer-requests/{id}', [TransferRequestController::class, 'update']);
Route::get('/transfer-requests/export/csv', [TransferRequestController::class, 'csvExport'])
    ->middleware('auth:sanctum');

Route::post('/update-password', [UserController::class, 'updatePassword'])
    ->middleware('auth:sanctum');
