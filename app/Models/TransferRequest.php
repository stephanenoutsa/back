<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferRequest extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_name',
        'sender_email',
        'recipient_name',
        'recipient_phone',
        'payment_proof',
        'original_amount',
        'original_currency',
        'converted_amount',
        'converted_amount_with_commission',
        'target_currency',
        'status',
        'verification_details',
        'verification_proof',
        'process_details',
        'process_proof'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'request_verified_at' => 'datetime',
        'request_processed_at' => 'datetime',
        'request_rejected_at' => 'datetime',
    ];
}
