<?php

namespace App\Exports;

use App\Models\TransferRequest;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransferRequestsExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return TransferRequest::query();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Sender Name',
            'Sender E-mail',
            'Recipient Name',
            'Recipient OM/MoMo Number',
            'Payment Proof',
            'Original Amount',
            'Original Currency',
            'Converted Amount',
            'Converted Amount With Commission',
            'Status',
            'Verification Details',
            'Verification Proof',
            'Process Details',
            'Process Proof',
            'Verification Date',
            'Process Date',
            'Rejection Date',
            'Request Date',
        ];
    }

    /**
    * @var TransferRequest $transferRequest
    */
    public function map($transferRequest): array
    {
        return [
            $transferRequest->id,
            $transferRequest->sender_name,
            $transferRequest->sender_email,
            $transferRequest->recipient_name,
            $transferRequest->recipient_phone,
            $transferRequest->payment_proof,
            $transferRequest->original_amount,
            $transferRequest->original_currency,
            $transferRequest->converted_amount,
            $transferRequest->converted_amount_with_commission,
            $transferRequest->status,
            $transferRequest->verification_details,
            $transferRequest->verification_proof,
            $transferRequest->process_details,
            $transferRequest->process_proof,
            Carbon::parse($transferRequest->request_verified_at),
            Carbon::parse($transferRequest->request_processed_at),
            Carbon::parse($transferRequest->request_rejected_at),
            Carbon::parse($transferRequest->created_at),
        ];
    }
}
