<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\AccountCreated;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $searchKey = $request->query('key');

            $users = User::where(function ($query) use ($searchKey) {
                if ($searchKey != null) {
                    return $query->where('first_name', 'LIKE', '%' . $searchKey . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $searchKey . '%')
                        ->orWhere('middle_name', 'LIKE', '%' . $searchKey . '%');
                }
            })
            ->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->get();

            Log::info('Users searched', [
                'users' => $users,
                'search_key' => $searchKey
            ]);

            return response()->json($users);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for users', [
                'error' => $e,
                'search_key' => $searchKey
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'unique:users', 'email:rfc,dns'],
                'role' => ['required', Rule::in(['verifier', 'processor'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Saving user validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $middleName = $request->middle_name;
            $email = $request->email;
            $role = $request->role;
            $password = Str::random(8);

            // Populate a user object
            $user = new User([
                'first_name' => $firstName,
                'middle_name' => $middleName,
                'last_name' => $lastName,
                'email' => $email,
                'role' => $role,
                'password' => Hash::make($password)
            ]);

            // Save the user and return a message
            if ($user->save()) {
                Log::info('User saved successfully', ['user_id' => $user->id]);

                // Send email to user with their password
                Mail::to($email)->send(new AccountCreated($firstName, $role, $password));

                return response()->json([
                    'message' => __('User saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the user');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving user by ID', ['user_id' => $id]);

            return response()->json(User::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve user by ID', [
                'user_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'email:rfc,dns'],
                'role' => ['required', Rule::in(['verifier', 'processor'])]
            ]);

            if ($validator->fails()) {
                Log::warning('Updating user validation request failed.', [
                    'user_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $middleName = $request->middle_name;
            $lastName = $request->last_name;
            $email = $request->email;
            $role = $request->role;

            // Update user
            $affected = User::where('id', $id)
                ->update([
                    'first_name' => $firstName,
                    'middle_name' => $middleName,
                    'last_name' => $lastName,
                    'email' => $email,
                    'role' => $role,
                ]);

            if ($affected) {
                Log::info('User updated successfully.', ['user_id' => $id]);

                return response()->json([
                    'affected_rows' => $affected
                ]);
            }

            Log::error('An error occurred when updating the user');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'old_password' => ['required', 'string', 'max:255'],
                'new_password' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating user password validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $oldPassword = $request->old_password;
            $newPassword = $request->new_password;

            $authUser = auth()->user();

            if (Hash::check($oldPassword, $authUser->password)) {
                $authUser->password = Hash::make($newPassword);

                if ($authUser->save()) {
                    Log::info('User password updated successfully.', ['user_id' => $authUser->id]);

                    return response()->json($authUser);
                }

                Log::error('An error occurred when updating the user password');

                return response()->json(
                    [
                        'message' => __('An error occurred when updating the user password')
                    ],
                    500
                );
            }

            Log::error('The password entered does not match the password saved for this user');

            return response()->json(
                [
                    'message' => __('The password entered does not match the password saved for this user')
                ],
                403
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the user password', [
                'error' => $e,
                'user' => $authUser
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the user
            $user = User::find($id);

            // Delete the user
            $user->forceDelete();

            Log::info('User deleted successfully', ['user_id' => $id]);

            return response()->json([
                'message' => __('User deleted successfully')
            ]);
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
