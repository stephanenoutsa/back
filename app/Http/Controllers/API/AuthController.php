<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * Login user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            try {
                // Create user token
                $token = auth()->user()->createToken(config('app.name'));

                Log::info('User logged in successfully.', ['id' => auth()->user()->id]);

                return response()->json(
                    [
                        'token' => $token->plainTextToken,
                        'user' => auth()->user()
                    ],
                    200
                );
            } catch (\Throwable $e) {
                Log::error('An error occurred when trying to login the user', ['error' => $e]);

                return response()->json($e, 500);
            }
        } else {
            Log::warning('User attempted to login with invalid credentials.', ['email' => $request->email]);

            return response()->json(
                [
                    'message' => __("Unauthorized")
                ],
                401
            );
        }
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            // Delete the user's token
            auth()->user()->currentAccessToken()->delete();

            Log::info('Logged out user', ['id' => auth()->user()->id]);

            return response()->json(
                [
                    'success' => 'true',
                ],
                200
            );
        } catch (\Throwable $e) {
            Log::error('Failed to logout user', ['id', auth()->user()->id]);

            return response()->json($e, 500);
        }
    }
}
