<?php

namespace App\Http\Controllers\API;

use App\Exports\TransferRequestsExport;
use App\Http\Controllers\Controller;
use App\Mail\TransferRequested;
use App\Mail\TransferRequestProcessed;
use App\Mail\TransferRequestRejected;
use App\Mail\TransferRequestSent;
use App\Mail\TransferRequestVerified;
use App\Models\TransferRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class TransferRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $status = $request->query('status');

            $requests = TransferRequest::where(function ($query) use ($status) {
                if ($status != null) {
                    return $query->where('status', $status);
                }
            })
            ->orderBy('updated_at', 'desc')
            ->get();

            Log::info('Transfer requests searched', [
                'transfer-requests' => $requests,
                'status' => $status
            ]);

            return response()->json($requests);
        } catch (\Throwable $e) {
            Log::error('An error occurred when listing the transfer requests', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'sender_name' => ['required', 'string', 'max:255'],
                'sender_email' => ['required', 'email:rfc,dns'],
                'recipient_name' => ['required', 'string', 'max:255'],
                'recipient_phone' => ['required', 'string', 'max:9'],
                'original_amount' => ['required'],
                'original_currency' => ['required', 'string', 'max:3'],
                'converted_amount' => ['required'],
                'converted_amount_with_commission' => ['required'],
                'target_currency' => ['required', 'string', 'max:3']
            ]);

            if ($validator->fails()) {
                Log::error('An error occurred when validating the request placement', ['error' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $senderName = $request->sender_name;
            $senderEmail = $request->sender_email;
            $recipientName = $request->recipient_name;
            $recipientPhone = $request->recipient_phone;
            $paymentProof = $request->payment_proof || '';
            $originalAmount = $request->original_amount;
            $originalCurrency = $request->original_currency;
            $convertedAmount = $request->converted_amount;
            $convertedAmountWithCommission = $request->converted_amount_with_commission;
            $targetCurrency = $request->target_currency;

            // Populate new transfer request object
            $transferRequest = new TransferRequest([
                'sender_name' => $senderName,
                'sender_email' => $senderEmail,
                'recipient_name' => $recipientName,
                'recipient_phone' => $recipientPhone,
                'payment_proof' => $paymentProof,
                'original_amount' => $originalAmount,
                'original_currency' => $originalCurrency,
                'converted_amount' => $convertedAmount,
                'converted_amount_with_commission' => $convertedAmountWithCommission,
                'target_currency' => $targetCurrency,
                'status' => 'pending'
            ]);

            // Save the transfer request and return a message
            if ($transferRequest->save()) {
                Log::info('Transfer request saved successfully', ['transfer_request_id' => $transferRequest->id]);

                // Send email to admins
                $admins = User::get();

                foreach($admins as $admin) {
                    Mail::to($admin)->send(new TransferRequested($admin->first_name, $transferRequest));
                }

                // Send email to sender
                Mail::to($senderEmail)->send(new TransferRequestSent($transferRequest));

                return response()->json([
                    'message' => __('Transfer request saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the user');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the transfer request')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the transfer request', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving transfer request by ID', ['transfer_request_id' => $id]);

            return response()->json(TransferRequest::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve transfer request by ID', [
                'transfer_request_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'sender_name' => ['required', 'string', 'max:255'],
                'sender_email' => ['required', 'email:rfc,dns'],
                'recipient_name' => ['required', 'string', 'max:255'],
                'recipient_phone' => ['required', 'string', 'max:9'],
                'original_amount' => ['required'],
                'original_currency' => ['required', 'string', 'max:3'],
                'converted_amount' => ['required'],
                'converted_amount_with_commission' => ['required'],
                'target_currency' => ['required', 'string', 'max:3'],
                'status' => ['required', Rule::in('pending', 'rejected', 'verified', 'processed')],
                'verification_details' => ['nullable', 'string'],
                'verification_proof' => ['nullable'],
                'process_details' => ['nullable', 'string'],
                'process_proof' => ['nullable'],
                'reject_reason' => ['nullable']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating transfer request validation request failed.', [
                    'transfer_request_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $senderName = $request->sender_name;
            $senderEmail = $request->sender_email;
            $recipientName = $request->recipient_name;
            $recipientPhone = $request->recipient_phone;
            $originalAmount = $request->original_amount;
            $originalCurrency = $request->original_currency;
            $convertedAmount = $request->converted_amount;
            $convertedAmountWithCommission = $request->converted_amount_with_commission;
            $targetCurrency = $request->target_currency;
            $status = $request->status;
            $verificationDetails = $request->verification_details;
            $verificationProof = null;
            if ($request->hasFile('verification_proof') && $request->file('verification_proof')->isValid()) {
                $verificationProof = $request->verification_proof->store('proofs/verified');
            }
            $processDetails = $request->process_details;
            $processProof = null;
            if ($request->hasFile('process_proof') && $request->file('process_proof')->isValid()) {
                $processProof = $request->process_proof->store('proofs/processed');
            }
            $rejectReason = $request->reject_reason;

            // Retrieve transfer request to update
            $transferRequest = TransferRequest::find($id);

            // Update transfer request
            $transferRequest->sender_name = $senderName;
            $transferRequest->sender_email = $senderEmail;
            $transferRequest->recipient_name = $recipientName;
            $transferRequest->recipient_phone = $recipientPhone;
            $transferRequest->original_amount = $originalAmount;
            $transferRequest->original_currency = $originalCurrency;
            $transferRequest->converted_amount = $convertedAmount;
            $transferRequest->converted_amount_with_commission = $convertedAmountWithCommission;
            $transferRequest->target_currency = $targetCurrency;
            $transferRequest->status = $status;

            if ($verificationDetails != null && $status == 'verified') {
                $transferRequest->verification_details = $verificationDetails;
                $transferRequest->verification_proof = $verificationProof;
            }

            if ($processDetails != null && $status == 'processed') {
                $transferRequest->process_details = $processDetails;
                $transferRequest->process_proof = $processProof;
            }

            if ($rejectReason != null && $status == 'rejected') {
                $transferRequest->reject_reason = $rejectReason;
            }

            if ($status == 'verified') {
                $transferRequest->request_verified_at = now();
            }

            if ($status == 'processed') {
                $transferRequest->request_processed_at = now();
            }

            if ($status == 'rejected') {
                $transferRequest->request_rejected_at = now();
            }

            if ($transferRequest->save()) {
                // Get admins
                $admins = User::get();

                switch($status) {
                    case 'verified':
                        // Send email to Processors
                        foreach($admins as $admin) {
                            if ($admin->role == 'processor') {
                                Mail::to($admin)->send(new TransferRequestVerified($admin->first_name, $id));
                            }
                        }

                        break;

                    case 'processed':
                        // Send email to all admins
                        foreach($admins as $admin) {
                            Mail::to($admin)->send(new TransferRequestProcessed($admin->first_name, $transferRequest));
                        }

                        // Send email to sender
                        Mail::to($transferRequest->sender_email)->send(new TransferRequestProcessed($transferRequest->sender_name, $transferRequest));

                        break;

                    case 'rejected':
                        // Send email to all admins
                        foreach($admins as $admin) {
                            Mail::to($admin)->send(new TransferRequestRejected($admin->first_name, $transferRequest));
                        }

                        // Send email to sender
                        Mail::to($transferRequest->sender_email)->send(new TransferRequestRejected($transferRequest->sender_name, $transferRequest));

                        break;

                    default:
                        break;
                }

                Log::info('Transfer request updated successfully.', ['transfer_request_id' => $id]);

                return response()->json($transferRequest);
            }

            Log::error('An error occurred when updating the transfer request');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the transfer request')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the transfer request', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    public function csvExport()
    {
        try {
            $results = Excel::download(new TransferRequestsExport, 'transfer_requests.csv');

            Log::info('Transfer request exported successfully', ['user_id' => auth()->id()]);

            return $results;
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to export transfer requests', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the transfer request
            $transferRequest = TransferRequest::find($id);

            // Delete the transfer request
            $transferRequest->delete();

            if ($transferRequest->trashed()) {
                Log::info('Transfer request deleted successfully', ['transfer_request_id' => $id]);

                return response()->json([
                    'message' => __('Transfer request deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the transfer request');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the transfer request')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the transfer request', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
