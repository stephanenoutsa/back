<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferRequestVerified extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user's name.
     *
     * @var String
     */
    public $name;

    /**
     * The request ID.
     *
     * @var Integer
     */
    protected $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.transfers.verified', [
            'id' => $this->id
        ]);
    }
}
