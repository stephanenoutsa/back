<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user's name.
     *
     * @var String
     */
    public $name;

    /**
     * The user's role.
     *
     * @var String
     */
    protected $role;

    /**
     * The user's password.
     *
     * @var String
     */
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $role, $password)
    {
        $this->name = $name;
        $this->role = ucwords($role);
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.accounts.created', [
            'name' => $this->name,
            'role' => $this->role,
            'password' => $this->password
        ]);
    }
}
