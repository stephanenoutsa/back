<?php

namespace App\Mail;

use App\Models\TransferRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferRequestSent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The request instance.
     *
     * @var TransferRequest
     */
    protected $transferRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransferRequest $transferRequest)
    {
        $this->transferRequest = $transferRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.transfers.sent', [
            'request' => $this->transferRequest
        ]);
    }
}
