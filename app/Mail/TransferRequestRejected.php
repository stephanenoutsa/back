<?php

namespace App\Mail;

use App\Models\TransferRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TransferRequestRejected extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user's name.
     *
     * @var String
     */
    public $name;

    /**
     * The request instance.
     *
     * @var TransferRequest
     */
    protected $transferRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, TransferRequest $transferRequest)
    {
        $this->name = $name;
        $this->transferRequest = $transferRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.transfers.rejected', [
            'request' => $this->transferRequest
        ]);
    }
}
